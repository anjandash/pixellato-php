<html>

<head>

<title>Pixellato | Subscriptions</title>

<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">

<link rel="stylesheet" type="text/css" href="style.css">

<link rel="shortcut icon" type="image/png" href="images/favicon.png">

<script src="lib/jquery-1.7.2.js" type="text/javascript"></script>


<script>
	 $(function() {
    		$('#activator4').click(function(){
        		$('#overlays4').fadeIn('fast',function(){
            		$('#boxs4').animate({'top':'80px'},500);
        		});
    		});
    		$('#boxclose4').click(function(){
        		$('#boxs4').animate({'top':'-500px'},500,function(){
            		$('#overlays4').fadeOut('fast');
        		});
   		});

		});

</script>

</head>

<body>



<div class="header" style="background: white;">


		<a href="index.php"><div class="header-input-logo" style="float:left; font-family:Arial; color: grey;"></div></a>


		<div class="header-input"><a href="index.php" style="color:grey;">HOME</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>



</div>


<div class="banner">
	<p style="padding-top:30px;">Subscriptions</p>
</div>

<!--<div style="margin-top: 10px; margin-left:595px; height:20px; width:280px; background:transparent; background-size:contain; background-repeat:no-repeat;">
</div>
<div align="center" style="width:1350px; min-width:100%; margin-top:10px;"><h2 style="color:grey;">Pricing for Agencies</h2></div>-->

				<div align="center" style="width:100%; height:620px;">
		    		<script src="https://gumroad.com/js/gumroad.js"></script>
		    		<div style="width:1320px; min-width:100%; height:20px; margin-top:20px;"><!--<p style="color:grey; font-family:Arial; font-size:14px;">Before subscribing we recommend you read the <a href="how.php" target="_blank" style="text-decoration:none; color:orangered; opacity:0.8;">how it works</a> page once.<p>--></div>
		    		<div style="width:900px; height:510; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px; background:whitesmoke; margin-top:20px; padding-top:20px;">
		    			<div style="height:40px; font-family:arial;; color:grey; font-size:20px; margin-left:-20px;"><h5>Monthly pricing for content producers</h5></div>
		    			
		    			<div class="subbox1" style="">
		    			<div class="subdetail" style="height:250px; width:100%;">
			    			<div style="height:100px; width:100%; border-bottom: 1px solid lightgrey; background:url(images/lite.jpg); background-size:contain;">
			    			</div>
			    			<div style="height:100px; width:100%; background:url(images/plite.jpg); background-size:contain; background-repeat:none; background-position:center;">
			    			</div>

			    			<ul align="left" style="list-style-type:none; color:#B0B0B0; margin-top:-5px; margin-left:-10px;">
			    			<li>50 High-Res images /mo.	</li>					
							<li>Dedicated Account Manager</li>
							</ul>
						</div>
		    			
						<a class="gumroad-butto" style="padding:20px; border:1px solid orangered; color:orangered;" href="https://gum.co/bGAuH"  target="_blank" data-gumroad-single-product="true">Lite Plan</a>
						</div>

						<div class="subbox1" style="">
						<div class="subdetail" style="height:250px; width:100%;">
							<div style="height:100px; width:99%; border: 1px solid lightgrey; background:url(images/regular.jpg); background-size:contain;">
							</div>
							<div style="height:100px; width:100%; background:url(images/pregular.jpg); background-size:contain; background-repeat:none; background-position:center;">
							</div>

							<ul align="left" style="list-style-type:none; color:#A0A0A0; margin-top:-5px; margin-left:-10px;">
							<li>150 High-Res images /mo.	</li>					
							<li>Dedicated Account Manager</li>
							<li>1 Free Photo Collection /mo.</li>
							</ul>
						</div>
		    			
						<a class="gumroad-butto" style="padding:20px; border:1px solid royalblue; color:royalblue;" href="https://gum.co/cEFaz" target="_blank" data-gumroad-single-product="true">Regular Plan</a>
						</div>

						<div class="subbox1" style="">
						<div class="subdetail" style="height:250px; width:100%;">
							<div style="height:100px; width:100%; border-bottom: 1px solid lightgrey; background:url(images/advanced.jpg); background-size:contain;">							
							</div>
							<div style="height:100px; width:100%; background:url(images/padvanced.jpg); background-size:contain; background-repeat:none; background-position:center;">							
							</div>

							<ul align="left" style="list-style-type:none; color:grey; margin-top:-5px; margin-left:-10px;">
							<li>300 High-Res images /mo.	</li>					
							<li>Dedicated Account Manager</li>
							<li>24x7 On-Call Support</li>
							<li>2 Free Photo Collections /mo.</li>
							</ul>
						</div>
		    			
						<a class="gumroad-butto" style="border:1px solid #009900; color:#009900;" href="https://gum.co/aszQX" target="_blank" data-gumroad-single-product="true">Advanced Plan</a>
						</div>
						<div style="width:inherit; height:20px; margin-top:20px;"><p style="color:grey; font-family:Arial; font-size:12px;">Need more images? We got you covered! <a href="support.php" target="_blank" style="text-decoration:none; color:black; opacity:0.8;">Let us know.</a> Create your own custom plan.<p></div>
				
				</div>	
				</div>

				
				<div align="center" class="getstarted" style="opacity:0.8; width:1320px; min-width:100%; height:200px; margin:0px 0px 60px 0px;;border-top:1px solid transparent; background: white; background-size: 100% 350px; background-repeat: no-repeat; background-attachment:;">
		    		<div style="width:1000px; height:100px; margin-top:40px; color:grey; border:1px solid transparent; border-top:1px solid lightgrey;">
		    			
		    			<div style="margin-top:20px; float:left;">
		    			<p align="left" style="font-family:arial; font-size:24px; line-height:150%;">
		    			 Or start with our Free Plan. Authentic, user-generated CC.0 images.<br/>Handpicked and tailored to your content.
		    			</p>
		    			</div>

		    			<div style="float:right; margin-top:-10px;">
		    				<div style="padding-top:45px;">			   				
			   				<a href="javascript:void(0)" style="" class="activator4" id="activator4"><div id="indexpack3" style="">Start Free Plan</div></a>
			   				</div>
		    			</div>

		    			
		    		</div>
		    	</div>

		    	<div class="overlays4" id="overlays4" style="display:none;"></div>

	  			<div class="boxs4" id="boxs4">
			 		<a class="boxclose4" id="boxclose4"></a>			 		
			 				<div style="width:100%; padding-top: 20px;">
			 					<div align="center" style="height:60px; width:400px; background:transparent; margin-left: 50px;">
			 						<img src="images/logozino.png" style="max-width: 100%; max-height: 100%;">
			 					</div>
			 					<div align="center" style="padding-top:20px;">
			 						
			 						<p style="font-family:Arial; font-weight:bold; margin-bottom:5px;">Start with the Pixellato Free Plan</p>
			 						<p style="font-family:Arial; font-size: 12px; margin-bottom:20px;">cancel anytime | no questions asked</p>
								  	<form action="requestsample.php" method="POST" style="">
									<input type="text" style="height:40px; border:1px solid lightgrey; border-radius:5px;"; name="email" size="64" maxlength="64" placeholder="Your email" required/><br/><br/>
									<input type="text" style="height:40px; border:1px solid lightgrey; border-radius:5px;"; name="company" size="64" maxlength="64" placeholder="Your Name/Organization"/><br/><br/>
									<input id="ques" type="submit" style="margin-top:5px;" name="request" value="Start Free">
									</form>

									<!--<p style="margin-top:10px; font-family:rounded; font-size:12px;">no questions asked | no strings attached</p>-->
								</div>
					  		</div>
					
					
				</div>


				<?php include('footer.php'); ?>

</body>

</html>