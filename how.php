<?php 

$messages="";

if(isset($_POST['send'])){
  // Change this to YOUR address

  $recipient = 'support@pixellato.com';

  $email = $_POST['email'];

  $body = $_POST['body'];

  $subject = "";

  # We'll make a list of error messages in an array


  if ($messages=="") {        

  	  mail($recipient,

      	$subject,

      	$body,

      	"From: <$email>\r\n" .

      	"Reply-To: <$email>\r\n");     

        $messages="Your message was sent successfully!";

        $email="";

		$body="";



  } else {

    # Send the email - we're done

  		$messages="Sending Failure. Please Try Again!";



  }



}



?>





<html>



<head>



<title>Pixellato | How It Works</title>



<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">



<link rel="stylesheet" type="text/css" href="style.css">



<link rel="shortcut icon" type="image/png" href="images/favicon.png">



<style>

	h4{

		color: grey;

	}

</style>



</head>



<body>







<div class="header" style="background: white;">



		<a href="index.php"><div class="header-input-logo" style="float:left; font-family:Arial; color: grey;"></div></a>



		<div class="header-input"><a href="index.php" style="color:grey;">HOME</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>



		



</div>





<div class="banner">

	<p style="padding-top:30px;">How It Works</p>

</div>


				<div align="center" style="width:1320px; min-width:100%; height:700px; padding:30px 0px; border-top:1px solid whitesmoke; background:white; margin-top:-5px;">

		    	

					<div style="height:430; width: 1300px; border:1px solid transparent; border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px; font-size:14px; color:grey;">

						<div class="singlehow" style="color:grey;">
						<div style="float:right; clear:left; border:4px solid lightgrey; margin-top:60px;">
						</div>

							<div style=" height:100px; width:100px; background:url(images/icons/request1.png); background-size:cover; margin:15px;">
							</div>

						
							<div style="border: 1px solid transparent; height:250px; width: 250px; font-family:rounded;">

							<br/><h3>SET REQUEST</h3><br/>
							<p style="text-align:center;">
							Your assigned account manager from pixellato contacts you immediately upon subscription - or on activation of the Free Plan. <br/><br/>
							You then set forward your requests depending upon your content requirements, story line, 
							or based on the kind of themes or emotions you are seeking in the photographs.</p><br/>						
								
							</div>
						</div>

						<div class="singlehow" style="color:grey;">
						<div style="float:right; clear:left; border:4px solid lightgrey; margin-top:60px;">
						</div>
						<div style="float:left; border:4px solid lightgrey; margin-top:60px;">
						</div>

							<div style=" height:100px; width:100px; background:url(images/icons/search1.png); background-size:cover; margin:15px;">
							</div>

							<div style="border: 1px solid transparent; height:250px; width: 250px; font-family:rounded;">

							<br/><h3>SEARCH | SCOUT | CREATE</h3><br/>
							<p style="text-align:center;">
							Our editors search our database for pre-licensed photographs and subsequently release all requests
							to our <span style="font-size:14px;">community of photographers</span> contributing on pixellato. <br/><br/>
							Editors then browse through the top-rated finds and carefully select the images that
							match your requirements, closely based on your directions.</p><br/>								
								
							</div>
						</div>

						<div class="singlehow" style="color:grey;">
						<div style="float:left; clear:right; border:4px solid lightgrey; margin-top:60px;">
						</div>
						<div style="float:right; border:4px solid lightgrey; margin-top:60px;">
						</div>
						
							<div style=" height:100px; width:100px; background:url(images/icons/rollover1.png); background-size:cover; margin:15px;">
							</div>

							<div style="border: 1px solid transparent; height:250px; width: 250px; font-family:rounded;">
								
								<br/><h3>SELECTION &amp; ROLLOVERS</h3><br/>
								<p style="text-align:center;">
								The watermarked versions of the selected images are then sent over to you securely for you select and finalize.<br/><br/>
								We quickly provide rollover images for any number of images that you would like to 
								swap. We also throw in a few extra content-relevant images with every request.</p><br/>								
									
							</div>
						</div>

						<div class="singlehow" style="color:grey;">
							<div style="float:left; clear:right; border:4px solid lightgrey; margin-top:60px;">
							</div>
							<div style=" height:100px; width:100px; background:url(images/icons/approval1.png); background-size:cover; margin:15px;">
							</div>

							<div style="border: 1px solid transparent; height:250px; width: 250px; font-family:rounded;">
								
								<br/><h3>APPROVAL &amp; DELIVERY</h3><br/>
								<p style="text-align:center;">
								Once all images are approved by you, we deliver the high-res versions of the selected images 
								securely in a password protected zipped folder.<br/><br/>
								We then process addtional or new requests if any. If you are a subscriber you can set new requests anytime via your account manager as long as you have
								additional image requirements.</p><br/>

							</div>
						</div>

					</div>
				


						<div align="center" style="height:250px; width:600px; font-family:Arial; color:grey; border-top:1px solid lightgrey; margin-top:50px;">
								<p><br/><br/></p>

								<div style="background: white; color:grey; padding:20px; padding-bottom:40px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
								<h4 style="padding-bottom:10px;">Content Producer or an Agency?</h4> <br/>
								<p style="font-size:14px; font-family:rounded;"><a href="mailto:support@pixellato.com" target="_blank" style="color:Orangered;">Contact us</a> with your requirements, and we will get back to you in no time. 
								<br/>Working together to tell better stories through photographs.</p><br/>

								<p style="font-size:14px; font-family:rounded;">Visit the <a href="index.php" style="color:navy; font-size:13px;">home</a> page or <a href="subscriptions.php" style="color:navy; font-size:13px;">subscriptions</a> page for more information.</p>
								</div>
						</div>


						


</div>


<!--<div align="center" style="margin-top:20px; width:600px; font-family:arial; color:lightgrey;">

						<h4 align="center">Photographer?</h4><br/>

						Photographer? Get free hosting of your high-res images for life. 80% royalty on sales - the <i>highest</i> royalty in the industry. <a href="photgraphers.php" style="color:grey; text-decoration:underline; font-family:Arial; font-size:14px; padding:1px 0px 2px 0px;">Submit samples here</a>
</div>-->




<div align="center" style="width:1320px; min-width:100%; margin: 0px 0px 40px 0px;">

	<div style="width:650px; height:auto; font-family:Arial; padding-bottom:30px; color:grey; padding:40px;  border:3px solid whitesmoke;">



		<div align="left">



		<div align="center" style="font-size:14px; color:lightgrey;">Copyright of all images curated on pixellato belong to respective photographers</p></div>



		


		<div align="left" style="margin-top:5px;">

		<p align="left">

		<!--<h4 align="left">Subscribers</h4><br/>

		Visitors who choose to subscribe to our basic monthly plans receive a .(zip)ped folder containing a collection of fresh photographs every month based upon their chosen niche or category. The collection is selected by our in-house editors based on individual subscriber preferences<sup></sup>.

		Upon subscription, subscribers receive a curated collection on their chosen niche or category instantly and are also contacted by their dedicated account managers. Subscribers can communicate their preferences to their account managers as and when they please. 

		<br/><br/>-->



		



		<!--However, monthly subscribers who want to go through the image browsing and selection process all by themselves may choose do so, and they can use their unique subscriber code to download the (alloted number of) images based on their monthly plan for Free.--></p><br/>

		</div>

		</div>



		<div style="border:3px solid whitesmoke; padding:20px; padding-top:0px; margin-top:30px;">



		<br/>

		<?php



			

			if($messages!=""){ ?>



			<div align="center" style="width:1320px; max-width:100%;">

			<div style="width:300px; height:25px; background:orangered; color:white; padding:5px; padding-top:10px; opacity:0.6; font-family:arial; border:1px solid darkred; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;"><?php echo $messages; ?></div>

			</div>



			<?php } 



			?>

		<br/>

		<p style="font-size:16px;">Need more information? 

		Let's get in touch.</p> <br/><br/>

		<form action="" method="POST">

		<input type="text" style="height:40px; border:1px solid lightgrey; border-radius:5px;"; name="email" size="64" maxlength="64" placeholder="Your email" required/><br/><br/>

		<textarea name="body" style="border:1px solid lightgrey; background:whitesmoke;" rows="15" cols="60" value="" placeholder="Your Message" required></textarea><br/><br/>

		<input id="ques" type="submit" style="" name="send" value="Send">

		</form>

		</div>





	</div>

</div>



<?php include('footer.php'); ?> 



</body>



</html>