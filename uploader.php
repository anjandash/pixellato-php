<link rel="stylesheet" type="text/css" href="lib/js/file-upload.css">
<script type="text/javascript" src="lib/js/file-upload.js"></script>

<script>
	
	function valueChanged()
	{
	    if($('#termsbox').is(":checked"))   
	        $("#hidebox").show();
	    else
	        $("#hidebox").hide();
	}

</script>

<div style="visibilty:hidden;">
    <div id="lightup" class="white_content">

      <div style="margin-left:98%; height:20px; padding-bottom:22px; margin-top:3px;">
            <a href = "javascript:void(0)" style="font-family:kozuka; font-weight:bold; font-size: 1.5em;" 
              onclick ="document.getElementById('lightup').style.display='none'; document.getElementById('fadeup').style.display='none';"><img src="images/close.png" style="height:100%; width:80%; ">
            </a>
      </div>

            
      <div class="upbox"><br/>

        <div id="uploader">
                        
          <div class="preview" align="center">
            <div id='container' style="height:100%; width:auto;">
              <img style="height:auto; width:auto;" id="photo" src="images/dot.png" onload="" />
            </div>
          </div>

          <script type="text/javascript">
          
          $("#photo").load(function() {
            var width = $(this).width();
            var height = $(this).height();
            

              $('input[name=wa]').val(width);
                  $('input[name=ha]').val(height); 
            
          });
          </script>

          


          <div align="center" style="" id="upload-form">

            
            <form id="bro" action="upload.php?u=<?php echo $username; ?>" method="post" enctype="multipart/form-data">

              <div class="file-upload">
                <label for="file-upload">BROWSE</label>
                <input id="file-upload" type="file" name="uploaded[]" style="margin-top:-15px;" size="70" multiple accept="images/*" onchange="loadFile(event)" multiple/>
              </div>

             


                            <script>
                              var loadFile = function(event) {
                                var output = document.getElementById('photo');
                                var fileName = event.target.files[0].name;

                                photo.src = URL.createObjectURL(event.target.files[0]);
                              };  

                              $("photo").load(function(){
                                var height = $(this).height();
                                var width = $(this).width();
                              });

                            </script>
                            
              <p id="para" style="margin-top:15px;">IMAGE TITLE:<br><input id="upload-title" type="text" name="caption" size="30" style="border:1px solid white;" placeholder="Create an image title" required></p><br/>
              <div id="upload-info">
              <p>STORY:</p><textarea rows="10" cols="50" name="story" placeholder="Add the story behind this image"></textarea><br><br/>
              <p>HASHTAGS:</p><textarea rows="5" cols="50" name="hashtags" placeholder="#travel #explore #wanderlust #paris #LA"></textarea><br/>
              
              <!--             
              <p>LOCATION: <br><input type="text" name="city" size="30" placeholder=""></p><br/>
              <p>COUNTRY: <br><input type="text" name="country" size="30" placeholder=""></p>
              -->
              </div>
              <br/>

                <input type="hidden" name="x1" value="" />
                <input type="hidden" name="y1" value="" />
                <input type="hidden" name="x2" value="" />
                <input type="hidden" name="y2" value="" />
                <input type="hidden" name="w" value="" />
                <input type="hidden" name="h" value="" />
                <input type="hidden" name="wa" value="" />
                <input type="hidden" name="ha" value="" />

                <div id="upthis-message">

                      <a href = "javascript:void(0)" style="" 
                        onclick ="document.getElementById('upthis-message').style.display='none';">
                          <div id="cross" style=""></div>
                      </a>
                
                Please Drag on the selected Image<br/>
                to set the Standard Aspect Ratio Before uploading.<br/><br/>
                </div>
                
                <div align="center" style="font-family:abel; font-size:12px; margin-top:-2px; margin-bottom:20px; color:grey;"><input id="termsbox" type="checkbox" checked="checked" style="height:13px;" onchange="valueChanged()"> I agree to the <a href="pdf/pixellato-contributor-terms.pdf" style="color:darkblue;" target="_blank">Photographer License Agreement</a> and <a href="pdf/pixellato-terms.pdf" style="color:darkblue;" target="_blank">Terms</a></div>           
              
                <div id="hidebox" >
                <input id="upthis" type="submit" name="upload" value="UPLOAD" />
                
                </div>  
            </form>
          </div>
        </div>
      </div>
      </div>


      <div id="fadeup" class="black_overlay">
      </div>  
    </div>