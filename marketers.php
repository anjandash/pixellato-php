<?php
?>
<html lang='en'>
    <head>
        <title>Pixellato | Visual Storytelling</title>
		<meta name="description" content="Royalty free images, story-based photography for content producers. Visual storytelling for greater engagement.">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
        <meta name="msvalidate.01" content="2A472EE3E5AEAA44042F1112017CFA15" />
        <meta name="google-site-verification" content="ZwCrBNGl5DG6Lui3QLSZg6brb0GERlW98-Y2iegtifY" />

        
        <script src="lib/jquery-1.7.2.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="style.css">
    	<link rel="stylesheet" type="text/css" href="zipstyle.css">
   		<link rel="stylesheet" type="text/css" href="css/animate.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"> 


        <script>

		$(window).scroll(function() {    
		    var scroll = $(window).scrollTop();

		     
		    if (scroll >= 200) {
		        $("#info1x").addClass("animated fadeinup");
		    }
		});

						
		$(function(){


			$('#landsloganx1').click(function(){

				$('html, body').animate({
		        scrollTop: $("#landslogan2").offset().top
		    }, 2000);
				

			});

			$('#landslogan2').click(function(){

				$('html, body').animate({
		        scrollTop: $("#landslogan3").offset().top
		    }, 2000);
				

			});


			$('#landslogan3').click(function(){

				$('html, body').animate({
		        scrollTop: $("#afternoon").offset().top
		    }, 2000);
				

			});


			$('#afternoon').click(function(){

				$('html, body').animate({
		        scrollTop: $("#journey").offset().top
		    }, 2000);

			});


			$('#journey').click(function(){

				$('html, body').animate({
		        scrollTop: $("#salescopy1").offset().top
		    }, 2000);
				

			});


			$('#salescopy1').click(function(){

				$('html, body').animate({
		        scrollTop: $("#coffee").offset().top
		    }, 2000);
				

			});


		});


						
		</script>



		<script>

			$(window).scroll(function() {
			    if ($(this).scrollTop()) {
			        $('#toTop:hidden').stop(true, true).fadeIn();
			    } else {
			        $('#toTop').stop(true, true).fadeOut();
			    }
			});


			$(window).scroll(function() {
			    if ($(this).scrollTop() > 3000) {
			        $('#bitplus').stop(true, true).fadeOut();
			    } else {
			        $('#bitplus:hidden').stop(true, true).fadeIn();
			    }
			});


		</script>


    </head>

<body>


<!-- ############################################################################# -->

<div id="wrapper" style="">
<div align="center" id="landslogan" class="sec" style="">

	
	<div id="landslogan-hide">
	
			<div align="right" id="mainlogodiv" style="">
				<a href="index.php"><div id="mainlogo" style="width:155px; height:50px; background:url(images/logozino.png); background-size:contain; background-position:100% 100%;  background-repeat:no-repeat;"></div></a>
			</div>
	

			<div align="left" id="landslogan-text" style="">
				<div id="one0" style="">You</div>
				<div id="one1" style="">spend<br/></div><br/>
				<div id="one1a" style="">a lot of&nbsp;</div><div id="one2" style="">time</div><br/><br/>
				<div id="one3" style="">creating content</div>
			
				
			</div>
			<div id="landsloganx1" style="margin-top:70px; width:50px; height:25px; background:url(images/arrow.png); background-size:cover;">
			</div>
	</div>			
</div>




		<div align="center" id="landslogan2" class="sec" style="">

			<div align="center" id="landslogan2-hide" style="font-family:Arial; font-weight:bolder; font-size:48px; color:black;">

				<div id="lsh-2" style="">
				<p style="color:grey;">But did you know?</p>
				</div>
				<div id="boxinfo" style="background:orangered;">
					<div id="info1" style="padding:20px; color:white;">
						<p id="l1" style="">CONTENT WITH ENGAGING VISUALS</p>
						<p id="l1" style="">ON AN AVERAGE RECEIVE</p>
						<p id="l2" style=""><span id="stat" style="">94</span>%</p>
						<div>
						</div>
						<p id="l3a" style="">MORE VIEWS</p>
						<p id="l4a" style="">THAN PLAIN TEXT CONTENT</p>
					</div>
				</div>

				<div id="boxinfo" style="background:#FC0081;">
					<div id="info2" style="padding:20px; color:white;">
						<p id="l1" style="">CONVERSION RATES FOR COMPANIES USING VISUAL CONTENT ARE</p>
						<p id="l2" style=""><span id="stat" style="">7</span>X</p>
						<div>
						</div>
						<p id="l3" style="">HIGHER</p>
					</div>
				</div>

				<div id="boxinfo" style="background:#0DC4FD;">
					<div id="info3" style="padding:20px; color:white;">
						<p id="l1" style="">IF RELEVANT IMAGES ARE PAIRED WITH CONTENT PEOPLE RETAIN</p>
						<p id="l2" style=""><span id="stat" style="">65</span>%</p>
						<div>
						</div>
						<p id="l3" style="">OF THAT</p>
						<p id="l4" style="">EVEN THREE DAYS LATER</p>
					</div>
				</div>

				<div id="landsloganx" style="margin-top:40px; width:50px; height:25px; background:url(images/arrow.png); background-size:cover;">
				</div>
			</div>

			
		</div>




		<div align="center" id="landslogan3" class="sec" style="">

			<div id="landslogan3-hide" style="">
				<div>Now,</div>
				<div style="display:inline-block; vertical-align:middle;">help your readers</div> <div id="engage" style="">engage</div> <div style="display:inline-block; vertical-align:top;">with</div> 
				<div style="">the <span id="content" style="">content</span></div>				
				<div id="lsh-1" style="">you've <span style="color:grey; opacity:0.8;">worked so hard</span> to produce</div> 
				<br/>
				<div><span style="color:#FC0081; font-size:88px;">&nbsp;</span></div>
				<div id="lsh-3">don't just put out information, tell</div> <div id="stories" style=""><span style="color:orangered;">stories</span></div>

				<div id="landsloganx" style="margin-top:40px; width:50px; height:25px; background:url(images/arrow.png); background-size:cover;">
				</div>
			</div>

			
		</div>




		<div align="center" id="afternoon" class="sec" style="">

			<div id="afternoon-hide" style="">
			<p id="afternoon-text" style="">Can you <span style="color:gold;">feel</span> the leisurely calm<br/>in this afternoon?
			</p>

			<div id="landsloganx" style="margin-top:170px; width:50px; height:25px; background:url(images/arrow.png); background-size:cover;">
			</div>
			</div>	

			
		</div>




		<div align="center" id="journey" class="sec" style="">

			<div id="journey-hide" style="">
			<p id="journey-text" style="">can you feel the joy in the ride?
			</p>

			<div id="landsloganx" style="margin-top:20px; width:50px; height:25px; background:url(images/arrow-white.png); background-size:cover;">
			</div>
			</div>	

			

		</div>




		<div align="center" id="salescopy1" class="sec" style="">

			<div id="salescopy1-hide" style="">
			<p id="sc1-1" style="">Deliver great content with <span style="color:black;">story-based photography</span>
			</p>

			<p id="sc1-2" style="">Create <span id="experiences" style="">experiences</span> for your readers<br/>Not just paragraphs to consume
			</p>	

			<div id="landsloganx" style="margin-top:100px; width:50px; height:25px; background:url(images/arrow.png); background-size:cover;">
			</div>			
			</div>

			

		</div>



	
		<div align="center" id="coffee" class="sec" style="">
			
			<div align="center" id="coffee-hide" style="">

				<div align="center">				
					<p id="zipbox-top" style="color:whitesmoke; opacity:0.8;"><span style="font-size:31px;">G</span>et story-based photography relevant to your content</p>
				</div>
				
				<p style="font-family:rounded; font-weight:bold; font-size:8px; color:white; margin-top:30px; margin-bottom:0px;"></p>


				<div id="zipbox" style="">
				
					<div>



					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/black.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">You tell us the emotions you are seeking to deliver, the theme of your content or the specific story line.</div>					
					</div>

					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/picture.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">We study your content and deliver the most relevant and engaging images for your content.</div>
					</div>


					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; margin-right:0px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/money.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">Only when you are happy with the images, you pay for our curation services. Plans starting at $5/post.</div>
					</div>

					</div>

					<p style="margin-bottom:5px;"></p>
					

				</div>			
				
				
				
				<div id="zipbox-bottom" align="center" style="">
				<p id="zbb-1" style="">Helping you engage your audience with visual storytelling</p>
				<p id="zbb-2" style="">without ever compromising on relevance or quality</p>
				<!--<p id="zbb-3" style="">Get stunning imagery that converts</p>-->


					<!--<div id="learn-more" style="">
					<form action="requestsample.php" method="POST">
					<input type="text" id="zone" name="email" placeholder="Your email" style="" required>
					<input type="submit" name="coffeeweeklearn" id="learn-more-button" style="" value="LEARN MORE">
					</form>
					</div>-->

					<div id="trypix2" style="width: 200px; background:#FC0081; color:whitesmoke; opacity:0.95; padding:10px; margin-top:40px; font-family:Arial; cursor:pointer;
					box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -moz-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -webkit-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25);">
					Start a New Project
					</div>

				</div>

				
			</div>


		</div>



		<div align="center" class="sec" style="height:430px; border:0px solid transparent; padding:10px 0px; box-shadow: 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25);-moz-box-shadow: 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25);-webkit-box-shadow: 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25);">
			
			<p id="lets" style="">Let's<span style="color:orangered"> go!</span></p>
			<p id="lets2" style="font-family:rounded; color:lightgrey; padding:0px 0px 5px 0px;">Get stunning imagery that converts</p>
			<p id="lets3" style="font-family:rounded; color:grey;"><a href="contact.php" style="text-decoration:underline; color:orangered;">contact us</a> to learn more!</p>

			<div class="animated bounce infinite" style="height:200px; width:200px; opacity:1; background:url(images/pops.png); background-size:contain; background-repeat:no-repeat; background-position: 50% 100%; margin-top:30px;">
			</div>
		</div>


		<!--<div align="center" class="sec" style="height:auto; padding: 50px 0px; font-family:Arial; background:whitesmoke; box-shadow: inset 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25); -moz-box-shadow: inset 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25); -webkit-box-shadow: inset 0 0.0125em 0.6em 0 rgba(0, 0, 0, 0.25);">
			
			<div style="min-height:800px; height:auto;">
				<div id="vsc1" style="">
				<h1 id="vsc1h1" style="">Visual Storytelling in Action</h1>
				<p id="vsc1p" style="">story-based photography to engage readers</p>
				</div>

				<div id="vsc2" style="">

					<a href="http://www.copyblogger.com/remember-everything-evernote/">
					<div class="imhov2" style="background:url(images/examp/examp11.jpg); background-size:contain; background-position:center center;">
						<img src="images/examp/examp11.jpg" style="visibility:hidden; max-width:100%;">
					</div>
					</a>

					
				</div>

				<div id="vsc3" style="">
					<div style="height:25px; margin:10px 0px 15px 0px;">
						<p style="color:lightgrey; font-family:Arial; font-size:22px;">more posts</p>
					</div>

					<a href="http://tympanus.net/Development/ArticleIntroEffects/index2.html">
					<div class="imhov" style="height: auto; border:1px solid lightgrey; background:url(images/examp/examp4.jpg); background-size:contain; background-repeat:none; margin-bottom:10px;">
							<img src="images/examp/examp4.jpg" style="max-width:100%; visibility:hidden;">
					</div>
					</a>					
					
					<a href="http://www.copyblogger.com/shareable-content/">
					<div class="imhov" style="height: auto; border:1px solid lightgrey; background:url(images/examp/examp5.jpg); background-size:contain; background-repeat:none; margin-bottom:10px;">
							<img src="images/examp/examp5.jpg" style="max-width:100%; visibility:hidden;">
					</div>
					</a>	

					<a href="http://www.contiki.com/six-two/secret-camping-locations-around-world/">
					<div class="imhov" style="height: auto; border:1px solid lightgrey; background:url(images/examp/examp9.jpg); background-size:contain; background-repeat:none; margin-bottom:10px;">
							<img src="images/examp/examp9.jpg" style="max-width:100%; visibility:hidden;">
					</div>
					</a>			


				</div>
			</div>

		</div>-->


		<div id="endsec" align="center" class="sec" style="height:auto; padding: 0px 0px 80px 0px; background:white; background-size:cover; background-position:center center;  background-repeat:none; ">


			<div id="vsc4" style="">
				
				<div style="border-top: 1px solid whitesmoke; padding-bottom:10px;">
					<p  style="color:grey; font-family:arial; font-size:1.2em; margin:30px 0px 8px;">Responsible image curation for your content!</p>
					<p  style="color:grey; font-family:arial; font-size:14px; margin-bottom:40px;">Working together to create great stories with engaging imagery that stand out from the crowd</p>
					<p style="color:lightgrey; font-family:arial; font-size:0.83em;;">Fill out project form <span style="color:orangered;">></span> Enter a headline <span style="color:#0CD4FD">></span> Upload sample content</p><br/>
					
				</div>		
				<div style="">
					<div id="trypix" style="width:200px;height:auto; background:#FC0081; color:white; padding:10px; font-family:calibri; font-weight: bold; cursor:pointer;
					 box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -moz-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -webkit-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
					 Start New Project
					</div>
				</div>	

				<script>
				$('#trypix').click(function(){

					if($('#formex').height() < 200){
				    $('#formex').animate({height:'450'}, 500);
				    $('#formex').css('visibility','visible');
					}
					else{
					$('#formex').animate({height:'0'}, 500);
				    

				    	$('#formex')
						  .delay(5)
						  .queue(function (next) { 
						    $(this).css('visibility', 'hidden'); 
						    next(); 
						  });
					}
				});

				</script>

				<script>
				$('#trypix2').click(function(){

					$('html, body').animate({
				        scrollTop: $("#endsec").offset().top
				    }, 2000);


					if($('#formex').height() < 200){
				    $('#formex').animate({height:'450'}, 500);
				    $('#formex').css('visibility','visible');
					}
					else{
					$('#formex').animate({height:'0'}, 500);
				    

				    	$('#formex')
						  .delay(5)
						  .queue(function (next) { 
						    $(this).css('visibility', 'hidden'); 
						    next(); 
						  });
					}
				});
				</script>

				<script type="text/javascript">
				    $(document).ready(function(){
				        $('input[type="file"]').change(function(e){
				            var fileName = e.target.files[0].name;
				            document.getElementById('output-file').innerHTML= fileName;
				            document.getElementById('formex').style.height='470px';
				        });
				    });
				</script>
				
				<div id="formex" style="">
					<form action="requestsample2.php" method="POST" enctype="multipart/form-data">
					<p align="left" style="width:240px; margin:20px 0px 5px 0px;">Enter Your Name:</p>
					<input type="text" name="tname" style="width:240px;height:35px;" required>
					<p align="left" style="width:240px; margin:20px 0px 5px 0px;">Enter Your Email:</p>
					<input type="text" name="temail" style="width:240px;height:35px;" required><br/>

					<p align="left" style="width:240px; margin:20px 0px 5px 0px;">Enter a Headline:</p>
					<input type="text" name="theadline" style="width:240px;height:35px;">


					<div style="width: 240px; padding:5px 0px 10px; margin: 20px 0px; border:1px solid lightgrey;">
					<div align="center" style="font-size:14px; margin: 0px 0px 10px;"><div>Upload sample content</div> <div>(as .docx .pdf etc)</div></div>
					<label class="custom-file-upload" id="axz" onchange="document.getElementById('axz').style.color='limegreen'; document.getElementById('axzim').src='images/icons/upload-green.png';">
					    <input id="custom-file" name="tfile" type="file">
					    <img id="axzim" style="height:12px; width:12px;" src="images/icons/upload.png">&nbsp;File Upload
					</label>

					<div id="output-file" style="width:250px; margin:10px 0px 0px; overflow:hidden;">
					</div>				
					</div>

					<input id="projectsub" type="submit" name="tsubmit" style="height: 40px; padding-top: 1px;" value="Begin Project">			

					</form>
				</div>		
					
			</div>			
		</div>



		<div style="width:320px; padding:20px 0px; margin: auto; background-repeat: repeat-x repeat-y;">
			
		</div>
		




		<!-- **************************************************************************************************************************************** -->



		<!--<div align="center" style="width:160px; height:70px; padding:5px; background:rgba(0,0,0,0.35); border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; position:fixed; bottom:52px; right:20px;">
   			
   			<div style="height:20px; width:150px; color: grey; border:1px solid grey; padding:3px 0px; font-family:rounded; font-weight:bold; margin-bottom:10px; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
   				Photographers
   			</div>
   			<a href="members.php">
   			<div style="display:inline-block; vertical-align:top; padding:5px; border:1px solid black; color:whitesmoke; font-family:arial; font-weight:bold; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
   				log in
   			</div>
   			</a>
   			<a href="signup.php">
   			<div style="display:inline-block; vertical-align:top; padding:5px; border:1px solid black; color:whitesmoke; font-family:arial; font-weight:bold; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
   				sign up
   			</div>
   			</a>
   			
   		</div>-->

   		<div align="center" id="bitplus" style="width:120px; height:38px; padding:5px; background:rgba(0,0,0,0.35); border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; position:fixed; top:62px; right:15px;">


   				<div style="">
					<div id="trypix3" style="width:80px; height:auto; background:#FC0081; color:white; padding:10px; font-family:calibri; cursor:pointer; font-size: 14px; font-weight: bold; box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -moz-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); -webkit-box-shadow: 0 0.065em 0.3em 0 rgba(0, 0, 0, 0.25); border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px;">
					 +New Project
					</div>
				</div>	

   		</div>

   				<script>
				$('#trypix3').click(function(){

					$('html, body').animate({
				        scrollTop: $("#endsec").offset().top
				    }, 2000);


					if($('#formex').height() < 200){
				    $('#formex').animate({height:'450'}, 500);
				    $('#formex').css('visibility','visible');
					}
					else{
					$('#formex').animate({height:'0'}, 500);
				    

				    	$('#formex')
						  .delay(5)
						  .queue(function (next) { 
						    $(this).css('visibility', 'hidden'); 
						    next(); 
						  });
					}
				});

				</script>

   		<br/>
   		<br/>
   		<br/>
	   	<!-- <?php //include("footer.php"); ?> -->

	   	<div id="footer">

				<div id="foot1" align="center" style="">

					    			



						<div align="center" style="width:70%; min-width: 230px; max-width:300px; height:17px; padding:8px; color:grey; margin-top:70px; background:rgba(0,0,0,0.35); border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;">

								<a id="insta" href="http://www.instagram.com/pixellato_" target="_blank" style="font-family:rounded; font-size:14px;"><img src="images/ico/instagram.png"  style="height:15px; width:17px; margin-left:20px; opacity:0.6; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;" onmouseout="this.style.opacity=0.6" onmouseover="this.style.opacity=0.8"></a>&nbsp;&nbsp;&nbsp;&nbsp;

								<a id="twitt" href="http://www.twitter.com/pixellato_" target="_blank" style="font-family:rounded; font-size:14px;"><img src="images/ico/twitter.png"  style="height:15px; width:17px; opacity:0.6; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;" onmouseout="this.style.opacity=0.6" onmouseover="this.style.opacity=0.8"></a>&nbsp;&nbsp;&nbsp;&nbsp;

								<a id="pint"  href="http://www.pinterest.com/pixellato_" target="_blank" style="font-family:rounded; font-size:14px;"><img src="images/ico/pinterest.png"  style="height:15px; width:17px; opacity:0.6; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;" onmouseout="this.style.opacity=0.6" onmouseover="this.style.opacity=0.8"></a>

						</div>



						<div align="center" id="soleth" style="width:70%; min-width: 230px; height:20px; color:grey; margin-top:20px; padding:10px; max-width:300px; background:rgba(0,0,0,0.35); border-radius:10px; -moz-border-radius:10px; -webkit-border-radius:10px;">

						<a href="contact.php" target="_blank" style="font-family:rounded; font-size:14px;">Contact</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;

				        	<a href="pdf/pixellato-terms.pdf" target="blank" style="font-family:rounded; font-size:14px;">Terms</a>&nbsp;&nbsp;&nbsp;&nbsp;|&nbsp;&nbsp;&nbsp;&nbsp;				        	

				        	

				        	
				        	<a href="faq.php" target="_blank" style="font-family:rounded; font-size:14px;">FAQ</a>

						</div>	   			

		    		  		

				</div>



		    	<div id="foot2" align="center" style="">

		    		<a href="index.php"><p style="width:100px; font-family:rounded; color:#A0A0A0; font-size:12px; padding-top:15px; padding-left: 20px;">&copy; Pixellato</p></a>

		    	</div>

		</div>



</div>


		<div id="toTop" style="">
			<img src="images/ico/up2.png" style="height:18px; width:18px; margin: 5px 6px 7px 6px;">
			
		</div>   



</body>
   		

    <script>
    	$("#toTop").click(function() {
		    $('html, body').animate({
		        scrollTop: $("#wrapper").offset().top
		    }, 2000);
		});
    </script>
