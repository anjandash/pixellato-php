<?php 

$messages="";

if(isset($_POST['send'])){

  // Change this to YOUR address

  $recipient = 'support@pixellato.com';

  $email = $_POST['email'];

  $body = $_POST['body'];

  $subjet = "";

  # We'll make a list of error messages in an array




  if ($messages=="") {

        
  		mail($recipient,

      	$subject,

      	$body,

      	"From: <$email>\r\n" .

      	"Reply-To: <$email>\r\n"); 

        


        $messages="Your message was sent successfully!";



        $email="";

		$body="";

    

  } else {

    # Send the email - we're done

  		$messages="Sending Failure. Please Try Again!";

  }

}

?>

<html>

<head>

<title>Pixellato | FAQ</title>

<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">

<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="zipstyle.css">

<link rel="shortcut icon" type="image/png" href="images/favicon.png">

</head>

<body>



<div class="header" style="background: white;">

		<a href="index.php"><div class="header-input-logo" style="float:left; font-family:Arial; color: grey;"></div></a>

		<div class="header-input"><a href="index.php" style="color:grey; font-size:15px;">HOME</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>

		

</div>





<div class="banner">

			<p>FAQ</p>

</div>


<div align="center" style="min-width:100%;">

<div align="left" class="faq">



	<h2>Q: What is Pixellato?</h2>

	<br/>

	<div class="ans"><p><b>A:</b> Pixellato is an image curation and content analysis service, helping content producers and marketers find the most engaging images for their content. 
	Based on the specific needs of clients pixellato serves handpicked story-based photographs.</p></div>

	<br/>

	<br/>

	
	<!--<h2>Q: How do subscriptions with Pixellato work?</h2>

	<br/>

	<div class="ans"><p><b>A:</b> The individual steps of subscription workflow are outlined in the <a href="how.php">How It Works</a> page. Please refer to it for more details. Subscribers must subscribe and set requests in order for their
	dedicated account managers on pixellato to search and scout for the required images in our database, and also to release the request to the community of photographers contributing on pixellato. And then top-rated relevant images are shortlisted and delivered to the subscriber securely upon approval.</p></div>

	<br/>

	<br/>-->



	<h2>Q: Can I use the images purchased on pixellato for commercial purposes?</h2>

	<br/>

	<div class="ans"><p><b>A:</b> Once you have purchased images on pixellato, you may use your images on both personal and commercial projects, and wherever you will as long as you do not violate the terms in the <a href="pdf/pixellato-buyer-terms.pdf" target="_blank">Buyer License Terms</a>.</p></div>

	<br/>

	<br/>



	<h2>Q: Can I resell the images purchased on pixellato?</h2>

	<br/>

	<div class="ans"><p><b>A:</b> <i>Not really.</i> All images uploaded on Pixellato belong to their respective copyright owners or creators. You may use them for your personal or commercial projects without attribution for unlimited number of times, unless otheriwse specified, but you may not claim ownership of the images or attempt to resell them. </p></div>

	<br/>

	<br/>



	<h2>Q: As a photographer, how much royalty would I receive when I sell images with pixellato?</h2>

	<br/>

	<div class="ans"><p><b>A:</b> All creators and photographers receive a royalty of 80% on their sales - the <i>highest</i> royalty in the industry.</p></div>

	<br/>

	<br/>

	<br/>


	<div class="ans2" align="center">

		<br/>
		<?php

			
			if($messages!=""){ ?>

			<div align="center" style="max-width:100%;">
			<div style="width:240px; height:25px; background:orangered; color:white; padding:5px; padding-top:10px; opacity:0.6; font-family:arial; border:1px solid darkred; border-radius:3px; -moz-border-radius:3px; -webkit-border-radius:3px;"><?php echo $messages; ?>
			</div>
			</div>

			<?php } 

			?>
		<br/>
		<div>
		Want us to answer your questions here? 
		Let us know. <br/><br/>
		</div>
		<div>
		<form action="" method="POST">
			<input type="text" id="que1" style="height:40px; border:1px solid lightgrey; border-radius:5px;" name="email" maxlength="64" placeholder="Your email" required/><br/><br/>
			<textarea name="body" id="que2" style="border:1px solid lightgrey; background:whitesmoke; resize:none;" value="" placeholder="Your Question" required></textarea><br/><br/>
			<input type="submit" style="" name="send" value="Send">
		</form>
		</div>
	</div>



</div>
</div>

<div style="margin-bottom:100px;">
</div>

<?php include('footer.php'); ?>

</body>

</html>