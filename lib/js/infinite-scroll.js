var page = 1;


            $(window).scroll(function () {
                $('#more').hide();
                $('#no-more').hide();

                if($(window).scrollTop() + $(window).height() > $(document).height() - 200) {
                    $('#more').css("top","400");
                    $('#more').show();
                }
                if($(window).scrollTop() + $(window).height() == $(document).height()) {

                    $('#more').hide();
                    $('#no-more').hide();

                    page++;

                    var data = {
                        page_num: page
                    };

                    var actual_count = "<?php echo $actual_row_count; ?>";

                    if((page-1)* 12 > actual_count){
                        $('#no-more').css("top","400");
                        $('#no-more').show();
                    }else{
                        $.ajax({
                            type: "POST",
                            url: "data.php",
                            data:data,
                            success: function(res) {
                                $("#result").append(res);
                                console.log(res);
                            }
                        });
                    }

                }


            });
