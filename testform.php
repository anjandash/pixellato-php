

<div id="hidebox2" style="padding: 20px; max-width: 210px; border: 1px solid lightgrey; border-radius:5px; background: white;">
<div style="border:1px solid transparent; max-width:200px; margin:auto; margin-bottom: 20px;">


<form action="" method="POST" id="payment-form" class="form-horizontal">

  <noscript>
  <div class="bs-callout bs-callout-danger">
    <h4>JavaScript is not enabled!</h4>
    <p>This payment form requires your browser to have JavaScript enabled. Please activate JavaScript and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a> for more informations.</p>
  </div>
  </noscript>


  <!-- ###################################### -->

  <?php
	require 'payment/lib/Stripe.php';

	$error = '';
	$success = '';
		  
	if ($_POST) {
	  Stripe::setApiKey("sk_test_fnqdlLjptDUZfKYkDmDqsSsF");

	  try {
	    if (!isset($_POST['stripeToken']))
	      throw new Exception("The Stripe Token was not generated correctly");
	    Stripe_Charge::create(array("amount" => 3000,
	                                "currency" => "eur",
	                                "card" => $_POST['stripeToken']));
	    $success = '<div class="alert alert-success" style="background: lightgreen; color: green; padding: 5px; 
	    				border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; margin-bottom: 10px;">
	                <strong>Success!</strong> Your payment was successful.
					</div>';
	  }
	  catch (Exception $e) {
		$error = '<div class="alert alert-danger" style="background: #ffb499; color: orangered; padding: 5px; 
						border-radius: 3px; -moz-border-radius: 3px; -webkit-border-radius: 3px; margin-bottom: 10px;">
				  <strong>Error!</strong> '.$e->getMessage().'
				  </div>';
	  }
	}
	?>
  <div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span> </div>
  <span class="payment-success" style="font-family: calibri; font-size: 14px;">
  <?= $success ?>
  <?= $error ?>
  </span>
  
   <!-- ###################################### -->


    
    <!-- Card Holder Name -->
    <div class="form-group">
      <div class="col-sm-6">
        <input type="text" name="cardholdername" maxlength="70" placeholder="CARD HOLDER NAME" class="card-holder-name form-control" style="height:35px; max-width: 200px;">
      </div>
    </div>
    
    <!-- Card Number -->
    <div class="form-group">
      <div class="col-sm-6">
        <input type="text" id="cardnumber" maxlength="19" placeholder="CARD NUMBER" class="card-number form-control" style="height:35px; max-width: 200px;">
      </div>
    </div>
    
    <!-- Expiry-->
    <div class="form-group">
      <div class="col-sm-6">
        <div class="form-inline">
          <select name="select2" data-stripe="exp-month" class="card-expiry-month stripe-sensitive required form-control" 
          			style="height: 35px; width: 96px; background: whitesmoke; outline: none; color: grey; border: 1px solid lightgrey;">
            <option value="01" selected="selected">01</option>
            <option value="02">02</option>
            <option value="03">03</option>
            <option value="04">04</option>
            <option value="05">05</option>
            <option value="06">06</option>
            <option value="07">07</option>
            <option value="08">08</option>
            <option value="09">09</option>
            <option value="10">10</option>
            <option value="11">11</option>
            <option value="12">12</option>
          </select>
          <select name="select2" data-stripe="exp-year" class="card-expiry-year stripe-sensitive required form-control" 
          			style="height: 35px; width: 100px; background: whitesmoke; outline: none; color: grey; border: 1px solid lightgrey;">
          </select>
          <script type="text/javascript">
            var select = $(".card-expiry-year"),
            year = new Date().getFullYear();
 
            for (var i = 0; i < 12; i++) {
                select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
            }
        </script> 
        </div>
      </div>
    </div>
    
    <!-- CVV -->
    <div class="form-group">
      <div class="col-sm-3">
        <input type="text" id="cvv" placeholder="CVV" maxlength="4" class="card-cvc form-control" style="height:35px; max-width: 200px;">
      </div>
    </div>
    
    <!-- Important notice -->
    <!--<div class="form-group">
    <div class="panel panel-success">
      <div class="panel-heading">
        <h3 class="panel-title">Important notice</h3>
      </div>
      <div class="panel-body">
        <p>Your card will be charged 30€ after submit.</p>
        <p>Your account statement will show the following booking text:
          XXXXXXX </p>
      </div>
    </div>-->
    
    <!-- Submit -->
    <div class="control-group" style="margin-top: 20px;">
      <div class="controls">
        <center>
          <button class="btn btn-success" id="btn-zip" type="submit">Checkout</button>
        </center>
      </div>
    </div>

</form>



</div>
</div>