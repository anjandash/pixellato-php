<head>

<title>Pixellato | 404 Page</title>

<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="image/png" href="images/favicon.png">

<style>
	h4{		color: grey;
	}
</style>

</head>





<body>
<div class="header" style="background: white;">
		<a href="index.php"><div class="header-input-logo" style="float:left; font-family:Arial; color: grey;"></div></a>
		<div class="header-input"><a href="index.php" style="color:grey;">HOME</a>&nbsp;&nbsp;&nbsp;&nbsp;</div>

</div>











<div class="banner">

	<p style="padding-top:30px;">Page not found!</p>


</div>







<div align="center" style="min-width:95%; margin: 30px 0px 50px 0px;;">



	<div id="text404" style="">

		<div align="left">

		<div align="center" style="font-size:84px; color:lightgrey;">404<br/></p></div>

		<div align="center" style="margin-top:20px; margin-bottom: 0px;">

		<h4 align="center">Oops! The page you were looking for could not be found. </h4><br/>
		We are looking into this immediately. The page could be hiding somewhere within the internet. And it is probably having a good time not having shown up for work today!
		<br/>
		<br/>
		<br/>

		<p style="font-size:14px; font-family:rounded;"><a href="mailto:support@pixellato.com" target="_blank" style="color:Orangered;">Contact us</a> if you have further questions, queries or feedback. 

		<br/>Working together to tell better stories through stunning imagery!</p><br/>
		<br/>

		<a href="javascript:void(0)" style="color:grey; font-size:12px; text-decoration:underline;" onclick="history.go(-1);">back to previous page</a>
		<br/>







		







		

		</div>



		</div>













	</div>



</div>







<?php include('footer.php'); ?> 







</body>