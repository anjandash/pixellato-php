<?php

?>

<html lang='en'>
    <head>
        <title>Pixellato | Visual Storytelling</title>
		<meta name="description" content="Royalty free images, story-based photography for content producers. Visual storytelling for greater engagement.">
        <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0" />
        <meta name="msvalidate.01" content="2A472EE3E5AEAA44042F1112017CFA15" />
        <meta name="google-site-verification" content="ZwCrBNGl5DG6Lui3QLSZg6brb0GERlW98-Y2iegtifY" />

        
        <script src="lib/jquery-1.7.2.js" type="text/javascript"></script>
        <script src="lib/js/waypoints/lib/noframework.waypoints.min.js"></script>


        <script>
        	var images = ["images/pro/1.jpg","images/pro/2.jpg","images/pro/4-min.jpg"]
			var i = 0;

			setInterval(function() {
			      document.getElementById('landsec').style.backgroundImage = "url(" + images[i] + ")";

			      i = i + 1;
			      if (i == images.length) {	i = 0; }

			}, 1000);
        </script>



        <script>
   
			$(document).ready(function() {

				if($(window).width() >= 350){


						  var waypoint = new Waypoint({
						  element: document.getElementById('lsw1w1x'),
						  handler: function(direction) {
						    //alert("Basic waypoint triggered")
						    //$("#landsec2").addClass("animated fadeinup");
						    //document.getElementById("landsec2").style.margin = "-500px";

						    if(direction == "down"){						    	
						    	$("#landsec").animate({height:'550'}, 1000);						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec").animate({height:'650'}, 1000);
						    }			    
						}
						});
				     	
				     	var waypoint2 = new Waypoint({
						  element: document.getElementById('lswz4'),
						  handler: function(direction) {

						    if(direction == "down"){
						    	
						    	$("#landsec2").animate({height:'300'}, 500);
						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec2").animate({height:'350'}, 500);
						    }			    
						}
						});

				     	var waypoint3 = new Waypoint({
						  element: document.getElementById('lswz5'),
						  handler: function(direction) {

						    if(direction == "down"){
						    	
						    	$("#landsec3").animate({height:'300'}, 500);
						    	//$("#zipbox-top").addClass("animated fadeinup");
						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec3").animate({height:'350'}, 500);
						    }			    
						}
						});	

				}			


				if($(window).width() <= 349){


						  var waypoint = new Waypoint({
						  element: document.getElementById('lsw1w1x'),
						  handler: function(direction) {
						    //alert("Basic waypoint triggered")
						    //$("#landsec2").addClass("animated fadeinup");
						    //document.getElementById("landsec2").style.margin = "-500px";

						    if(direction == "down"){						    	
						    	$("#landsec").animate({height:'350'}, 1000);						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec").animate({height:'650'}, 1000);
						    }			    
						}
						});
				     	
				     	var waypoint2 = new Waypoint({
						  element: document.getElementById('lswz4'),
						  handler: function(direction) {

						    if(direction == "down"){
						    	
						    	$("#landsec2").animate({height:'180'}, 500);
						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec2").animate({height:'220'}, 500);
						    }			    
						}
						});

				     	var waypoint3 = new Waypoint({
						  element: document.getElementById('lswz5'),
						  handler: function(direction) {

						    if(direction == "down"){
						    	
						    	$("#landsec3").animate({height:'200'}, 500);
						    	//$("#zipbox-top").addClass("animated fadeinup");
						    	
						    }
						    else if(direction == "up"){
						    	$("#landsec3").animate({height:'250'}, 500);
						    }			    
						}
						});	

				}			
			});

        </script>

		<script>
			$(window).load(function(){
		   // PAGE IS FULLY LOADED  
		   // FADE OUT YOUR OVERLAYING DIV
		   $('#overlay-load').fadeOut();   
		   $('#wrapper').addClass('animated fadein');
		   $('#wrapper').css('display', 'block');
		   
			});
		</script>

		<script>

			$(window).scroll(function() {
			    if ($(this).scrollTop()) {
			        $('#toTop:hidden').stop(true, true).fadeIn();
			    } else {
			        $('#toTop').stop(true, true).fadeOut();
			    }
			});

		</script>

		<script>

			(function($) {
			    $.fn.goTo = function() {
			        $('html, body').animate({
			            scrollTop: (($(this).offset().top) - 200 ) + 'px'
			        }, 3000);
			        return this; // for chaining...
			    }
			})(jQuery);

			function scrollfaq(){
				$('#secfaq').goTo();
			}

			function scrollbase(){
				$('#coffeexo').goTo();
			}

		</script>



		       	
       	<link rel="stylesheet" type="text/css" href="style.css">
    	<link rel="stylesheet" type="text/css" href="zipstyle.css">
   		<link rel="stylesheet" type="text/css" href="css/animate.css">
        <link rel="shortcut icon" type="image/png" href="images/favicon.png"> 

    </head>  


    <body>

  	
    	<div id="wrapper2">
	    <div align="center" class="lander" style="">

	    <div align="center" id="overlay-load" style="width: auto; min-width:100%; height:101%; background:white;">
    		<div style="width:330px;  position: relative; top: 50%; -moz-transform: translateY(-50%); -webkit-transform: translateY(-50%);  -ms-transform: translateY(-50%);  transform: translateY(-50%);">
		     <img src="images/loading.gif" alt="Loading"/>
		     <p class="animated pulse infinite" style="color:lightgrey; font-family:rounded;"></p>
		     </div>
		</div>

	    <div style="background: rgba(0,0,0,0.2); height: 100%; width: 100%;">
	
			<div style="width:100%; height: 60px; background: rgba(0,0,0,0.85); padding-top: 2px;">
				<div id="logobase" onclick="location.href='index.php';">
					<div id="logoimg" onclick="location.href='index.php';"><img src="images/logo/icon.png" id="limg"></div>
					<div id="logotxt">
						<div>Pixellato</div>
					</div>
				</div>

				<div id="navext" onclick="scrollbase()"><img src="images/loaders/shift.png" id="barext"></div>
				<div id="navfaq" onclick="scrollfaq()" style="">FAQ</div>
				<a href="marketers.php"><div id="navagencies" style="">AGENCIES</div></a>
			</div>

			<div align="left" style="height: 120px;">		
			</div>


	    	<div align="center" class="headland">
	    		<p>Upload your <span id="hland1">best</span>
	    		<span id="hland2">photos</span><br/> <span id="hland3">create</span> revenue streams <span id="hland4">online</span>.</p>
	    	</div>

	    	<div>
	    		<div id="landb0" class="" onclick="location.href='signup.php';">Signup</div>
	    	</div>

	    	<div id="landlog">
	    		<div id="landb1">Already signed up?&nbsp;&nbsp;</div>
	    		<div id="landb2" onclick="location.href='members.php';">Login</div>
	    	</div>
  	
	    </div>
	    </div>

	    <div align="center" class="landsec" style="min-height: 300px; height: auto; 
			    box-shadow: inset 0 7px 29px -7px rgba(0,0,0,0.4); 
			    -webkit-box-shadow: inset 0 7px 29px -7px rgba(0,0,0,0.4);  
			    -moz-box-shadow: inset 0 7px 29px -7px rgba(0,0,0,0.4); 
	    		font-family: Arial; color: lightgrey; padding: 30px 0px; background: whitesmoke;
	    		">

	    		<div id="pisp">

	    		<div>help <span class="junk">marketers</span></div>
	    		<div>tell better <span class="junk">stories</span></div>
	    		<div style="margin:10px 0px; font-family: Arial; font-size: 36px; font-weight: bolder;">><</div>
	    		<div>get paid for your</div>
	    		<div><span class="junk">photographs</span></div>

	    		</div>


	    </div>

		<!--<div class="sec" style="height: 500px; width: 100%; background: white;">
			<div id="secheader" style="height: 100px; background: crimson;
					background-image: linear-gradient(to top right, plum, indigo);">
			</div>
		</div>-->

	    <div align="center" id="landsec" class="landsec" style="">

	    	<div style="background: rgba(0,0,0,0.2); height: 100%; width: 100%; padding: 30px 0px;">

	    		<div id="landsecwrap1" style="padding-top: 80px;">
		    		<div id="lsw1w1x">Shoot</div>

		    		<div id="lswx"> 
			    		<div id="lsw2w1">great&nbsp;</div>
			    		<div id="lsw2w2">photos</div>
		    		</div>

		    		<div id="lswz" style="height: 200px;">		    		
		    		</div>

		    		<div id="lswz2" style="height: 50px;">
		    		</div>

		    		<div id="lswz3" style="height: 50px;">
		    		</div>

		    		<div id="lswz4" style="height: 80px;">
		    		</div>	

		    		<div id="lswz5">
		    		</div>		    		



	    		</div>
	    	</div>

	    </div>



	    <div align="center" id="landsec2" class="landsec" style="background: whitesmoke;">


	    		<div id="landsecwrap2" style="">
		    		<div id="lsw4w1">Add photos</div>
		    		<div id="lsw4w2">&nbsp;to your</div>
		    		<div id="lsw4w3">profile</div>
		    		<div id="lsw4w4">on the</div>
		    		<div id="lsw4w5">go</div>

	    		</div>
	    </div>


	    <div align="center" id="landsec3" class="landsec" style="">

	    		<div id="landsecwrap1" style="">
		    		<div id="lsw1w1">Showcase</div>

		    		<div id="lsw2">
			    		<div id="lsw2w1x">your&nbsp;</div>
			    		<div id="lsw2w2x">work</div>
		    		</div>

		    		<div id="lsw3">
			    		<div id="lsw3w1">to the</div>
			    		<div id="lsw3w2">world</div>
		    		</div>
	    		</div>

	    </div>


	    <div id="secfaq" class="sec" style="height: auto; width: 100%; background: white;">
			<div id="secheader" style="height: 140px; background: crimson;
					background-image: linear-gradient(to top right, crimson, darksalmon);">
					<div style="font-family: rounded; font-size: 14px; color: white; padding: 25px 0px 2px 30px;">FREQUENTLY ASKED QUESTIONS</div>
					<div id="faqhq">Your Questions Answered</div>
			</div>
			<div id="secbody" style="padding: 50px 0px 60px 0px; border-bottom: 3px solid lightgrey; height: auto; text-align: center; background-image: url(images/pattern.png);">

				<div id="qbox" style="">
					<div class="qqwc">So how does it work?</div>
					<div class="qansas">Its like Instagram, you post photos, but then you set a price for each photo and you get paid when marketers buy them on pixellato.</div>
				</div>

				<div id="qbox" style="">
					<div class="qqwc">So do you think my photos could be relevant?</div>
					<div class="qansas">Yes. With high quality camera-enabled phones, users are taking better photos now more than ever. While marketing agencies are seeking new photos every week for their content. <br/><br/>The best-selling photos on pixellato are uploads on art, architecture, food, and travel.</div>
				</div>

				<div id="qbox" style="">
					<div class="qqwc">And do I need to have paypal or something?</div>
					<div class="qansas">Yes, right now we dispatch all payouts through paypal. But if you don't have a paypal account yet, don't worry. You can create your paypal account in your sweet time, whenever you want. And your payouts will just add up in your pixellato account over time.</div>
				</div>

				<div id="qbox" style="">
					<div class="qqwc">Then what happens when somebody buys my photo? How does the payment work?</div>
					<div class="qansas">Once an image is sold, you receive 90% of the payment amount (for a sale of $5 you receive a payout of $4.50) directly to your paypal account. You can always check your current balance under the earnings tab in the profile page.</div>
				</div>

				<div id="qbox" style="">
					<div class="qqwc">So what kind of photos should I add? What photos are needed?</div>
					<div class="qansas">It is your account. You may add what you wish! If you love cats, you are free to flood our platform with close-ups of kittens. But for pointers, you may want to start with the best-sellers. The best-selling photos are art, architecture, food, and travel posts.</div>
				</div>

			<!-- If somebody bought my photo, how much could i get? -->

			</div>
		</div>



		<div align="center" id="coffeexo" class="sec" style="">			
			<div align="center" id="coffee-hide" style="">

				<div align="center">				
					<p id="zipbox-top" style="font-family: arial; font-weight: normal; color:whitesmoke; opacity:0.8;"><span style="font-size:31px;">H</span>ow 
					<span style="">it works!</span></p>
				</div>
				
				<p style="font-family:rounded; font-weight:bold; font-size:8px; color:white; margin-top:30px; margin-bottom:0px;"></p>


				<div id="zipbox" style="">
				
					<div>
					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/p1.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">Upload your best photos on pixellato.</div>					
					</div>

					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/p2.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">Let us market your photos on autopilot.</div>
					</div>


					<div align="center" id="steps" style="">
						<div align="center" style="width:70px; height:70px; margin-right:0px; border:1px solid lightgrey; border-radius: 50%; -moz-border-radius: 50%; -webkit-border-radius: 50%;">
							<div style="height:40px; width:40px; margin-top:15px; background-image:url(images/ico/p3.png); background-size: contain;">
							</div>
						</div>
						<div style="width:200px; margin-top:20px;">Get revenues on the photos that sell.</div>
					</div>
					</div>

					<p style="margin-bottom:5px;"></p>				

				</div>			
				
				
				
				<div id="zipbox-bottom" align="center" style="">
				<p id="zbb-1" style="">Make the best of your photos!</p>
				<p id="zbb-2" style="">Signup to get started</p>
				<!--<p id="zbb-3" style="">Optimized for mobile photography</p>-->



					<div id="trypixhome" style="" onclick="location.href='signup.php';">
					Signup
					</div>

				</div>

				
			</div>


		</div>




	    <?php include "footer.php"; ?>




		</div>     


		<div id="toTop" style="">
			<img src="images/ico/up2.png" style="height:18px; width:18px; margin: 5px 6px 7px 6px;">
			
		</div>   


    </body>

    <script>
    	$("#toTop").click(function() {
		    $('html, body').animate({
		        scrollTop: $("#wrapper2").offset().top
		    }, 2000);
		});
    </script>
		

