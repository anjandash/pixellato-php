<?php
	session_start();
	include ("dbconfig.php"); 
	
	if(isset($_GET['post_id'])){
		$post = $_GET['post_id'];
	}
	else{
		?><script>window.location.href='404.php'; </script><?php
	}

	if(isset($_GET['oid'])){
		$orderid_time = $_GET['oid'];
	}
	else{
		$orderid_time = "";	
	}


	if (!isset($_SESSION["email_login"])) {
					$username = "";
					?><!--<script>window.location.href='members.php?log=unlog&&post_id=<?php echo $post; ?>'; </script>--><?php
					}
					else
					{
					$email = $_SESSION["email_login"];

					$sql2 = mysql_query("SELECT id, username, first_name FROM users WHERE email='$email'");
					$row2 = mysql_fetch_array($sql2);
					$username = $row2['username'];
					$fname = $row2['first_name'];

					}


	$dispage1 = "";
	$dispage2 = "";
	if (isset($_GET['log'])) {
	   $message = mysql_real_escape_string($_GET['log']);

	   if ($message=="failedlogin") {
	     $dispage1="Incorrect login details! <a href='support.php' style='text-decoration:underline; color:white' target='_blank'><i>forgot password?</i></a>";  
	   }
	   else if ($message=="registered"){
	   	 $dispage2="You are regitered now! Please log in to continue.";
	   }
	}


?>

<head>
<title>Pixellato | Checkout</title>
<meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0">


<link rel="stylesheet" href="payment/css/bootstrap-formhelpers-min.css" media="screen">
<link rel="stylesheet" href="payment/css/bootstrapValidator-min.css"/>
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" />

<style type="text/css">
.col-centered {
    display:inline-block;
    float:none;
    text-align:left;
    margin-right:-4px;
}
.row-centered {
	margin-left: 9px;
	margin-right: 9px;
}
</style>


<link rel="stylesheet" type="text/css" href="style.css">
<link rel="shortcut icon" type="image/png" href="images/favicon.png">
<link rel="stylesheet" type="text/css" href="css/imgareaselect-default.css" />  
<script src="inc/jquery-1.4.2.js"></script>  
<script type="text/javascript" src="scripts/jquery.imgareaselect.pack.js"></script> 



<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
<script src="payment/js/bootstrap-min.js"></script>
<script src="payment/js/bootstrap-formhelpers-min.js"></script>
<script type="text/javascript" src="payment/js/bootstrapValidator-min.js"></script>


<script>
	window.onload = function() {
		var d = new Date().getTime();
		document.getElementById("tid").value = d;
	};

		
</script>

<script>
	window.onload = function() {
    var src = document.getElementById("bemail"),
        dst = document.getElementById("cidentifier");
    src.addEventListener('input', function() {
        dst.value = src.value;
    });
};
</script>


<script>
	
	function valueChanged2()
	{
	    if($('#termsbox2').is(":checked"))   
	        $("#hidebox2").fadeIn();
	    else
	        $("#hidebox2").fadeOut();
	}


	 $(function() {
		    $('#activator').click(function(){
		        $('#overlays').fadeIn('fast',function(){
		            $('#boxs').animate({'top':'80px'},500);
		        });
		    });
		    $('#boxclose').click(function(){
		        $('#boxs').animate({'top':'-500px'},500,function(){
		            $('#overlays').fadeOut('fast');
		        });
		    });

		});

</script>



<script type="text/javascript">
$(document).ready(function() {
    $('#payment-form').bootstrapValidator({
        message: 'This value is not valid',
        feedbackIcons: {
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
		submitHandler: function(validator, form, submitButton) {
                    // createToken returns immediately - the supplied callback submits the form if there are no errors
                    Stripe.card.createToken({
                        number: $('.card-number').val(),
                        cvc: $('.card-cvc').val(),
                        exp_month: $('.card-expiry-month').val(),
                        exp_year: $('.card-expiry-year').val(),
			name: $('.card-holder-name').val(),
			address_line1: $('.address').val(),
			address_city: $('.city').val(),
			address_zip: $('.zip').val(),
			address_state: $('.state').val(),
			address_country: $('.country').val()
                    }, stripeResponseHandler);
                    return false; // submit from callback
        },
        fields: {
            street: {
                validators: {
                    notEmpty: {
                        message: 'The street is required and cannot be empty'
                    },
					stringLength: {
                        min: 6,
                        max: 96,
                        message: 'The street must be between 6 to 96 characters'
                    }
                }
            },
            city: {
                validators: {
                    notEmpty: {
                        message: 'The city is required and cannot be empty'
                    }
                }
            },
			zip: {
                validators: {
                    notEmpty: {
                        message: 'The zip is required and cannot be empty'
                    },
					stringLength: {
                        min: 3,
                        max: 9,
                        message: 'The zip must be more than 3 and less than 9 characters long'
                    }
                }
            },
            email: {
                validators: {
                    notEmpty: {
                        message: 'The email address is required and can\'t be empty'
                    },
                    emailAddress: {
                        message: 'The input is not a valid email address'
                    },
					stringLength: {
                        min: 6,
                        max: 65,
                        message: 'The email must be more than 6 and less than 65 characters long'
                    }
                }
            },
			cardholdername: {
                validators: {
                    notEmpty: {
                        message: 'The card holder name is required and can\'t be empty'
                    },
					stringLength: {
                        min: 6,
                        max: 70,
                        message: 'The card holder name must be between 6 to 70 characters'
                    }
                }
            },
			cardnumber: {
		selector: '#cardnumber',
                validators: {
                    notEmpty: {
                        message: 'The credit card number is required and can\'t be empty'
                    },
					creditCard: {
						message: 'The credit card number is invalid'
					},
                }
            },
			expMonth: {
                selector: '[data-stripe="exp-month"]',
                validators: {
                    notEmpty: {
                        message: 'The expiration month is required'
                    },
                    digits: {
                        message: 'The expiration month can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var year         = validator.getFieldElements('expYear').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < 0 || value > 12) {
                                return false;
                            }
                            if (year == '') {
                                return true;
                            }
                            year = parseInt(year, 10);
                            if (year > currentYear || (year == currentYear && value > currentMonth)) {
                                validator.updateStatus('expYear', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
            expYear: {
                selector: '[data-stripe="exp-year"]',
                validators: {
                    notEmpty: {
                        message: 'The expiration year is required'
                    },
                    digits: {
                        message: 'The expiration year can contain digits only'
                    },
                    callback: {
                        message: 'Expired',
                        callback: function(value, validator) {
                            value = parseInt(value, 10);
                            var month        = validator.getFieldElements('expMonth').val(),
                                currentMonth = new Date().getMonth() + 1,
                                currentYear  = new Date().getFullYear();
                            if (value < currentYear || value > currentYear + 100) {
                                return false;
                            }
                            if (month == '') {
                                return false;
                            }
                            month = parseInt(month, 10);
                            if (value > currentYear || (value == currentYear && month > currentMonth)) {
                                validator.updateStatus('expMonth', 'VALID');
                                return true;
                            } else {
                                return false;
                            }
                        }
                    }
                }
            },
			cvv: {
		selector: '#cvv',
                validators: {
                    notEmpty: {
                        message: 'The cvv is required and can\'t be empty'
                    },
					cvv: {
                        message: 'The value is not a valid CVV',
                        creditCardField: 'cardnumber'
                    }
                }
            },
        }
    });
});
</script>
<script type="text/javascript">
            // this identifies your website in the createToken call below
            Stripe.setPublishableKey('pk_test_zsX5MkJQOSp5wy7WvO7Tulgq');
 
            function stripeResponseHandler(status, response) {
                if (response.error) {
                    // re-enable the submit button
                    $('.submit-button').removeAttr("disabled");
					// show hidden div
					document.getElementById('a_x200').style.display = 'block';
                    // show the errors on the form
                    $(".payment-errors").html(response.error.message);
                } else {
                    var form$ = $("#payment-form");
                    // token contains id, last4, and card type
                    var token = response['id'];
                    // insert the token into the form so it gets submitted to the server
                    form$.append("<input type='hidden' name='stripeToken' value='" + token + "' />");
                    // and submit
                    form$.get(0).submit();
                }
            }
 

</script>
</head>


<body>

<!-- BEGINNING OF HEADER TWEAK -->

 <?php

if (isset($_POST["email_login"]) && isset($_POST["password_login"])) {
	$email_login = $_POST["email_login"];
	$password_login = preg_replace('/[^!<>@&\/\sA-Za-z0-9_]/', '', $_POST["password_login"]);

	$password_login_md5 = md5($password_login);
	$sql = mysql_query("SELECT id FROM users WHERE email='$email_login' AND password = '$password_login_md5'");
	$userCount = mysql_num_rows($sql);
	if($userCount == 1) {
		while ($row = mysql_fetch_array($sql)) {
			$id = $row["id"];
		}

		
		$_SESSION["email_login"] = $email_login;
		$sql2 = mysql_query("SELECT id, username FROM users WHERE email='$email_login'");
		$row2 = mysql_fetch_array($sql2);
		$username = $row2['username'];
		?><script>window.location.href='checkout.php?post_id=<?php echo $post; ?>'; </script><?php
		exit();
	}

	else{

		
		?><script>window.location.href='checkout.php?log=failedlogin&&post_id=<?php echo $post; ?>'; </script><?php

	}
	
}
?>

<?php include("inc/header.inc.php"); ?>


				

<!-- END OF HEADER TWEAK -->

		<div class="banner">
			<p>checkout</p>
		</div>



	<div style="max-width:100%; margin-bottom:50px;">


			<?php
				require 'payment/lib/Stripe.php';

					$error = '';
					$success = '';
											  
						if ($_POST) {
							Stripe::setApiKey("sk_test_fnqdlLjptDUZfKYkDmDqsSsF");

							try {
								if (!isset($_POST['stripeToken']))
									throw new Exception("The Stripe Token was not generated!");
									Stripe_Charge::create(array("amount" => 1000,
										   					"currency" => "usd",
										                    "card" => $_POST['stripeToken']));
										    $success = '<div align="center" class="alert alert-success" id="payalert-success">
										                <strong>Success!</strong> Your payment was successful.
														</div>

														<div style="width:110px; height:50px; margin: auto;">
														<a href="https://s3-us-west-2.amazonaws.com/pixellato/'.$post.'" download>
														<div align="center" style="padding:5px; padding-top:8px; height:20px; width:100px; background:cornflowerblue; color:white; border-radius:5px; -moz-border-radius:5px; -webkit-border-radius:5px; margin:auto; margin-top:15px; margin-bottom: 0px;">
														DOWNLOAD
														</div>
														</a>
														</div>';

									// INSERT SQL/PDO command here to insert the payment/charge amount/postid/username here

									$checkoutquery = @mysql_query("INSERT into payments (order_id, amount, postid, buyer) VALUES ('$orderid_time', '$amount', '$post', '$username')") or die (mysql_error());
							}
							catch (Exception $e) {
											$error = '<div align="center" class="alert alert-danger" id="payalert-danger">
													  <strong>Error!</strong> '.$e->getMessage().'
													  </div>';
								}
						}
			?>

			<div class="alert alert-danger" id="a_x200" style="display: none;"> <strong>Error!</strong> <span class="payment-errors"></span> </div>
			<span class="payment-success" style="font-family: calibri; font-size: 14px;">
				 <?= $success ?>
				 <?= $error ?>
			</span>


			<div align="center" id="paydisp">
			<div id="feedone" class="paybox" style="background:url(croploads/<?php echo $post; ?>); background-size:contain; background-repeat: no-repeat; margin-bottom: 0px;">
						<img id="zig" src="croploads/<?php echo $post; ?>">
			</div>
			</div>

				<div id="payslide">
					
					
							
							<div align="center" style="padding:20px; background: whitesmoke;">



									<div align="left" style="padding: 20px; max-width: 210px; border: 1px solid lightgrey; border-radius:5px; background: white; font-family: calibri;">
									
									<?php 
										if($orderid_time == "")
											$orderid_time=time(); 
										
										$amount= '$5';
									?>
									<div style="border: 1px solid whitesmoke; padding: 5px;">
									Order Id	:  <?php echo $orderid_time; ?>
									<br/>					
									Amount	:&nbsp;<?php echo $amount; ?>
									</div>
									
												       
				
						        	<div align="center" style="font-size:12px; margin-top:15px; color:grey;">
						        	<input id="termsbox2" type="checkbox" checked="checked" style="height:13px;" onchange="valueChanged2()">
						        	I agree to the <a href="pdf/pixellato-buyer-terms.pdf" style="color:darkblue;" target="_blank">Buyer License Agreement</a> 
						        	and <a href="pdf/pixellato-terms.pdf" style="color:darkblue;" target="_blank">Terms</a>
						        	</div>	  

						        	</div>

						        	<!-- START #################################################### --> 	




									<div id="hidebox2" style="padding: 20px; max-width: 210px; border: 1px solid lightgrey; border-radius:5px; background: white;">
									<div style="border:1px solid transparent; max-width:200px; margin:auto; margin-bottom: 20px;">


									<form action="checkout.php?oid=<?php echo $orderid_time; ?>&&post_id=<?php echo $post; ?>" method="POST" id="payment-form" class="form-horizontal">

									  <noscript>
									  <div class="bs-callout bs-callout-danger">
									    <h4>JavaScript is not enabled!</h4>
									    <p>This page requires your browser to have JavaScript enabled. Please activate JavaScript and reload this page. Check <a href="http://enable-javascript.com" target="_blank">enable-javascript.com</a> for more informations.</p>
									  </div>
									  </noscript>

								    
									    <!-- Card Holder Name -->
									    <div class="form-group">
									      <div class="col-sm-6">
									        <input type="text" name="cardholdername" maxlength="70" placeholder="CARD HOLDER NAME" class="card-holder-name form-control" style="height:35px; max-width: 200px;">
									      </div>
									    </div>
									    
									    <!-- Card Number -->
									    <div class="form-group">
									      <div class="col-sm-6">
									        <input type="text" id="cardnumber" maxlength="19" placeholder="CARD NUMBER" class="card-number form-control" style="height:35px; max-width: 200px;">
									      </div>
									    </div>
									    
									    <!-- Expiry-->
									    <div class="form-group">
									      <div class="col-sm-6">
									        <div class="form-inline">
									          <select name="select2" data-stripe="exp-month" class="card-expiry-month stripe-sensitive required form-control" 
									          			style="height: 35px; width: 96px; background: whitesmoke; outline: none; color: grey; border: 1px solid lightgrey;">
									            <option value="01" selected="selected">01</option>
									            <option value="02">02</option>
									            <option value="03">03</option>
									            <option value="04">04</option>
									            <option value="05">05</option>
									            <option value="06">06</option>
									            <option value="07">07</option>
									            <option value="08">08</option>
									            <option value="09">09</option>
									            <option value="10">10</option>
									            <option value="11">11</option>
									            <option value="12">12</option>
									          </select>
									          <select name="select2" data-stripe="exp-year" class="card-expiry-year stripe-sensitive required form-control" 
									          			style="height: 35px; width: 100px; background: whitesmoke; outline: none; color: grey; border: 1px solid lightgrey;">
									          </select>
									          <script type="text/javascript">
									            var select = $(".card-expiry-year"),
									            year = new Date().getFullYear();
									 
									            for (var i = 0; i < 12; i++) {
									                select.append($("<option value='"+(i + year)+"' "+(i === 0 ? "selected" : "")+">"+(i + year)+"</option>"))
									            }
									        </script> 
									        </div>
									      </div>
									    </div>
									    
									    <!-- CVV -->
									    <div class="form-group">
									      <div class="col-sm-3">
									        <input type="text" id="cvv" placeholder="CVV" maxlength="4" class="card-cvc form-control" style="height:35px; max-width: 200px;">
									      </div>
									    </div>
									    
									    <!-- Important notice -->
									    <!--<div class="form-group">
									    <div class="panel panel-success">
									      <div class="panel-heading">
									        <h3 class="panel-title">Important notice</h3>
									      </div>
									      <div class="panel-body">
									        <p>Your card will be charged 30€ after submit.</p>
									        <p>Your account statement will show the following booking text:
									          XXXXXXX </p>
									      </div>
									    </div>-->
									    
									    <!-- Submit -->
									    <div class="control-group" style="margin-top: 20px;">
									      <div class="controls">
									        <center>
									          <button class="btn btn-success" id="btn-zip" type="submit">Checkout</button>
									        </center>
									      </div>
									    </div>

									</form>



									</div>
									</div>

						        	<!-- END OF START ###################################################### -->
						        
						        	
						    </div>
						        
					   	
					
				</div>

	</div>





<div align="center"  id='nope-more' style="display:block; font-size:0.7em; opacity:0.8; background:#f0f0f0; height:50px; max-width:100%;">
	<a href="index.php"><p style="font-family:abel; color:grey; font-size:12px; padding-top:15px;">&copy; Pixellato</p></a>
</div>

</body>


